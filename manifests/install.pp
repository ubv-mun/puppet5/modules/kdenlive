# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include kdenlive::install
class kdenlive::install {
  package { $kdenlive::package_name:
    ensure => $kdenlive::package_ensure,
  }
}
